<?php 
require_once("template/template_upper.html");

switch($_GET["p"])
{
        case "home":
        default:
                require_once("template/template_home.html");
                break;

        case "contributors":
                require_once("template/template_contributors.html");
                break;

        case "examples":
                switch($_GET["s"])
                {
                        case "motivating":
                        default:
                                require_once("template/examples/template_motivating.php");
                                break;

                        case "basics":
                                require_once("template/examples/template_basics.html");
                                break;

                        case "runtimes":
                                require_once("template/examples/template_runtimes.php");
                                break;

                        case "multiplelaunches":
                                require_once("template/examples/template_multiplelaunches.php");
                                break;

                        case "hypervisor":
                                require_once("template/examples/template_hypervisor.php");
                                break;
                }
                break;
}

require_once("template/template_bottom.html");
?>
