  function collap_callback_open(element)
  {
        var str = element.firstElementChild.innerHTML.replace(/<i class=\"large material-icons\">add<\/i>/g, "<i class=\"large material-icons\">remove</i>");
        element.firstElementChild.innerHTML = str;
  }

  function collap_callback_close(element)
  {
        var str = element.firstElementChild.innerHTML.replace(/<i class=\"large material-icons\">remove<\/i>/g, "<i class=\"large material-icons\">add</i>");
        element.firstElementChild.innerHTML = str;
  }

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {
            onOpenStart : collap_callback_open,
            onCloseStart : collap_callback_close});
  });
