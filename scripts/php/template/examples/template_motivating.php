<h1 class="center teal-text text-darken-3">Motivating Examples</h1>
<h3 class="center teal-text text-darken-4">Quick code to understand SwLoc !</h3>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <span class="card-title teal-text text-darken-4">The goal</span>
                                <p>The aim of SwLoc is to easily clusterize parallel codes with a minimum of modifications of your original codes. <p>
                                <p>Here, an example of what we can do with SwLoc :</p>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card teal darken-4 z-depth-3">
                        <div class="card-content white-text">
                                <span class="card-title white-text">File example.cpp</span>
                                <pre>
                                        <code class="language-c">
<?php 
        $content = file_get_contents("template/examples/website_runtime.cpp");
        echo htmlentities($content);
?>
                                        </code>
                                </pre>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <p>Interested ? Let's start with some basic examples !</p>
                        </div>
                        <div class="center-align">
                                <p><a class="waves-effect waves-light btn-floating btn-large teal accent-3" href="?p=examples&s=basics"><i class="large material-icons">send</i></a><br /><br /></p>
                        </div>
                </div>
        </div>
</div>
