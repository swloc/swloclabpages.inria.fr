<h1 class="center teal-text text-darken-3">Multiple Launches</h1>
<h3 class="center teal-text text-darken-4">How to launch SwLoc several times</h3>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <span class="card-title teal-text text-darken-4">SwLoception !</span>
                                <p>Maybe, you use a library which already uses SwLoc and you didn't know it or you want to have a better granularity ?</p>
                                <p>It is possible to use SwLoc in SwLoc !</p>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12 center-align">
                <img style="height:500px" src="./img/archi_swloc.svg" alt="SwLoc Architecture" />
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <span class="card-title teal-text text-darken-4">Try it !</span>
                                <p>Here, an example to create some SwLoc contexts inside a SwLoc context :</p>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card teal darken-4 z-depth-3">
                        <div class="card-content white-text">
                                <span class="card-title white-text">File example.cpp</span>
                                <pre>
                                        <code class="language-c">
<?php 
        $content = file_get_contents("template/examples/website_multiple_launches.c");
        echo htmlentities($content);
?>

                                        </code>
                                </pre>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <p>Now, let's try to manipulate resources between contexts !</p>
                        </div>
                        <div class="center-align">
                                <p><a class="waves-effect waves-light btn-floating btn-large teal accent-3" href="?p=examples&s=hypervisor"><i class="large material-icons">send</i></a><br /><br /></p>
                        </div>
                </div>
        </div>
</div>
