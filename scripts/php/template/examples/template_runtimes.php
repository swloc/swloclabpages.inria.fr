<h1 class="center teal-text text-darken-3">Step by Step Examples</h1>
<h3 class="center teal-text text-darken-4">Transform easily your code to use SwLoc !</h3>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <span class="card-title teal-text text-darken-4">Runtimes !</span>
                                        <p>SwLoc is compatible with some runtimes like OpenMP or Intel TBB. You just need to activate it by adding the appropriate parameter when creating the context.</p>
                                        <p> Here, an example with 2 contexts : one using OpenMP and the other, Intel TBB.</p>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card teal darken-4 z-depth-3">
                        <div class="card-content white-text">
                                <span class="card-title white-text">File example.cpp</span>
                                <pre>
                                        <code class="language-c">
<?php 
        $content = file_get_contents("template/examples/website_runtime.cpp");
        echo htmlentities($content);
?>
                                        </code>
                                </pre>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <span class="card-title teal-text text-darken-4">Easiness</span>
                                        <p>As you can see, no modification of your original code is necessary to run it with SwLoc !</p>
                        </div>
                </div>
        </div>
</div>

<div class="row">
        <div class="col s12">
                <div class="card white z-depth-3">
                        <div class="card-content teal-text text-darken-3">
                                <p>Next step : SwLoception !
                        </div>
                        <div class="center-align">
                                <p><a class="waves-effect waves-light btn-floating btn-large teal accent-3" href="?p=examples&s=multiplelaunches"><i class="large material-icons">send</i></a><br /><br /></p>
                        </div>
                </div>
        </div>
</div>
